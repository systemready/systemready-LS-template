# Running SystemReady LS tests.

## Background and limitations

The SystemReady [SR Architecture Compliance Suite 1.0 (SR ACS 1.0)](https://github.com/ARM-software/arm-systemready/tree/main/SR) is designed to test compliance to the SBSA and SBBR requirements for SystemReady SR certification. At this time, the SR ACS 1.0 tests are also leveraged to for [SystemReady LS v0.9](https://www.arm.com/architecture/system-architectures/systemready-certification-program/ls) certification. This includes testing compliance to the SBSA and LBBR requirements for SystemReady LS.

Some of the SR ACS tests depend on having the UEFI pre-boot environment, including the UEFI Shell, which is not required to be present for LBBR compliance. This limitation is addressed by using an SBBR compliant firmware on the same system under LS certification to run the UEFI SBSA tests. Once tests, the firmware is then switched over to LBBR compliant LinuxBoot firmware for the final SBSA/LBBR tests in Linux.

This guide will illustrate the testing procedure used for SystemReady LS certification.

---

## Setup 

SystemReady LS testing is based on the [sr\_acs\_live\_image 1.0 found here](https://github.com/ARM-software/arm-systemready/tree/main/SR/prebuilt_images). This image will need to be flashed on a drive accessible by both SBBR & LBBR compliant firmware.

---
## SBBR firmware steps

Flash the system with a SBBR compliant firmware, and boot into the UEFI Shell. Escape any startup script:
```
UEFI Interactive Shell v2.2
EDK II
UEFI v2.70
Mapping table
      FS2: Alias(s):HD1b:;BLK4:
          VenHw(100C2CFA-B586-4198-9B4C-1683D195B1DA)/HD(1,MBR,0x8F3D8FCA,0x800,
0x773800)
...
     BLK0: Alias(s):
          PcieRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/USB(0x1,0x0)
Press ESC in 5 seconds to skip startup.nsh or any other key to continue.
Shell> 
```

Then, locate the first partition of the SR ACS image, and move into the SBSA directory (\EFI\BOOT\bsa\sbsa):

```
Shell> FS0:
FS0:\> cd EFI\BOOT\bsa\sbsa
FS0:\EFI\BOOT\bsa\sbsa\> ls
Directory of: FS0:\EFI\BOOT\bsa\sbsa\
02/16/2022  20:22 <DIR>         4,096  .
02/16/2022  20:22 <DIR>         4,096  ..
02/16/2022  20:22             462,848  Sbsa.efi
          1 File(s)     462,848 bytes
          2 Dir(s)

```

From here, you can manually run the SBSA tests, with the following command:

```
FS0:\EFI\BOOT\bsa\sbsa\> Sbsa.efi -skip 800 -f SbsaResults.log
```
This will create a log file called SbsaResults.log, that we use for part in the Certification, please place it in **`./manual-results/sbsa-uefi/`** . No other tests are run with the SBBR compliant firmware.

---

## LBBR steps

Flash the LBBR compliant firmware onto the system under test.

### ACS
After the system has turned on, the firmware should drop you into a U-root shell or similar first stage linux environment. From here, you can mount and boot the ACS drive, which may be an `/dev/sdXX` drive, `/dev/nvmeX` drive, or similar, depending on your system's setup. Once mounted you can execute a kexec to start the ACS linux environment:

`kexec -d -i ramdisk-busybox.img -l Image -c "rootwait verbose debug crashkernel=256M"`


```
2021/10/05 00:22:19 Welcome to u-root!
   _   _      _ __ ___   ___ | |_
  | | | |____| '__/ _ \ / _ \| __|
  | |_| |____| | | (_) | (_) | |_
   \__,_|    |_|  \___/ \___/ \__|

~/# mkdir mnt
~/# mount /dev/sda1 /mnt
~/# cd mnt/
~/mnt# kexec -d -i ramdisk-busybox.img -l Image -c "rootwait verbose debug crashkernel=256M"
......
.................
2021/10/05 00:24:35 Kernel: /tmp/kexec-image660720805
2021/10/05 00:24:35 Initrd: /tmp/kexec-image806163904
2021/10/05 00:24:35 Command line: rootwait verbose debug crashkernel=256M

```

Finally, run `kexec -e` to execute the kexec into the Linux ACS image.

The tests should automatically run and generate 
* `SbsaResultsApp.log`, `SbsaResultsKernel.log` in the ACS_Results partition of the  ACS drive, these should be placed in **`./manual-results/sbsa-linux/`**
* `FWTSResults.log` in the ACS_Results partition of the ACS drive this should be place **`./manual-results/fwts/`**


### Testing Linux OSs

At this time, `Debian`, `Ubuntu`, `Fedora`, `CentOS`, and `openSUSE` have been tested with LinuxBoot firmware. This is not an exhaustive list of all possible OSes that a system can be certified with. The results may very with different OS and system configurations. 

A checklist of the OS logs is listed in the next section. Results from at least 2 different distribution need to be collected for certification. 

The tested Operating systems must boot on the system using the LBBR firmware. OS images may need to be pre-installed to the drive, for example by copying a raw disk image or installing the OS with SBBR firmware first. An EXT4 filesystem is recommended for testing to simplify the kexec step.

The OS boot process is similar to booting the ACS linux image, but it may vary for each distributions. 

Examples of the OS kexec commands:

```
   _   _      _ __ ___   ___ | |_
  | | | |____| '__/ _ \ / _ \| __|
  | |_| |____| | | (_) | (_) | |_
   \__,_|    |_|  \___/ \___/ \__|

~/# mkdir mnt
~/# mount /dev/nvme0n1p2 /mnt/
~/# cd /mnt/boot
~/# kexec -d -i initrd.img-5.10.0-12-arm64 -l vmlinuz-5.10.0-12-arm64 -c "ro root=/dev/nvme0n1p2"
~/# kexec -e
```

### `./os-logs/`

#### `./os-logs/[distroname] [distroversion]/`
- Please check ./os_logs/OS_image_download_links.txt to get the latest release for testing
- Please rename the subfolders. For example, rename 'linux_distro_1' to 'Fedora Server 35'

- Install the OS to a disk, and boot it. (Note that a network controller (PCIe or USB) is needed.)
- The install log must begin when the platform is released from reset and must include:
    - All firmware output
    - Output of boot into installed OS.
- Run the following commands and attach the logs: (Note that some commands may not work on some distros, and you may need to install the command or run an alternative command.)
```
    sudo -s                      (Prevent from running into "Permission denied" error)
    dmesg
    lspci -vvv
    lscpu
    lsblk
    lsusb
    dmidecode
 	free   
	uname -a
    cat /etc/os-release
    ifconfig                    (If ifconfig is not supported, use "ip addr show" and "ip link show" instead.)
                                (If network port doesn't get IP address, run "dhclient" or "ip a add <assigned IP address>/255.255.255.0 dev eth<number>" and "ip link set dev eth<number> up")
    ping -c 5 www.arm.com       (If the system is connected to intranet, please ping another system's IP address)
    Go to /sys/firmware and run "ls -lR" (Or use tree /sys/firmware/)

```
- Copy the entire content of /sys/firmware and attach zipped/archive file. You can run "cp -r sys/firmware <Target folder path>" and then separately compress the files.
   - Note that directly running "tar -cvzf /<Target folder path>/sys_firmware.tgz /sys/firmware/" may result in corruption.


---

## Testing Log Checklist

The following log files must be collected as part of the certification submission:

* `SbsaResults.log` SBSA test from UEFI Shell tests (SBBR firmware is accepted for this log only)
* `SbsaResultsApp.log` & `SbsaResultsKernel.log` SBSA tests from Linux tests (LBBR firmware)
* `FWTSResults.log` from Linux tests (LBBR firmware)
*  Logs from 2 or more Linux OS distros
	* `dmesg` boot log
	* `lspci -vvv` log
	* `lscpu` log
	* `lsblk` log
	* `lsusb` log
	* `dmidecode` log
	* `free` log
	* `uname -a` log
	* `cat /etc/os-release` (or similar) log
	* `ifconfig` (or similar) log
	* `ping` log
	* copy of the `/sys/firmware` file tree
